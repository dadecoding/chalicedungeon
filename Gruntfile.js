module.exports = function(grunt) {
  require('jit-grunt')(grunt);

  grunt.initConfig({

    less: {
      main: {
        files: { 'source/compiled/css/style.css': 'source/css/style.less' }
      }
    },

    copy: {
      bootstrapCSS: {
        expand: true, flatten: true,
        src: [
          'node_modules/bootstrap/dist/css/bootstrap-theme.min.css',
          'node_modules/bootstrap/dist/css/bootstrap.min.css'
          ],
        dest: 'source/compiled/css/'
      },
      mainJS: {
        expand: true, flatten: true,
        src: [
          'node_modules/bootstrap/dist/js/bootstrap.min.js',
          'node_modules/jquery/dist/jquery.min.js'
        ],
        dest: 'source/compiled/js/'
      }
    },

    concat: {
      mainCSS: {
        src: ['source/compiled/css/**.css'],
        dest: 'public/css/style.min.css'
      },
      mainJS: {
        src: [
          'source/compiled/js/jquery.min.js',
          'source/compiled/js/bootstrap.min.js'
          ],
        dest: 'public/js/app.min.js'
      }
    },

    imgcompress: {
      options: {
        optimizationLevel: 3,
        progressive: true,
        duplication: 'override',
      },
      dist: {
        files: {
          'public/images/': 'source/images'
        }
      }
    },

    watch: {
      main: {
        files: ['source/css/style.less'],
        tasks: ['less:main', 'concat:mainCSS']
      }
    }

  });

  grunt.registerTask('default', ['less', 'copy', 'concat', 'watch']);
}
