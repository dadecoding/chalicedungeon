var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var dungeonsSchema = new Schema({
  chalice_id: String,
  chalice: String,
  rite: String,
  glyph: String,
  level1_weapons: String,
  level2_weapons: String,
  level3_weapons: String,
  level4_weapons: String,
  //level1_materials: Array,
  level1_materials: {
    type: Schema.ObjectId,
    ref: 'materials'
  },
  level2_materials: String,
  level3_materials: String,
  level4_materials: String,
  note: String
});

mongoose.model('dungeons', dungeonsSchema);
