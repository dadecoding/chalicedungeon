var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', {
      title: 'Bloodborne - Chalice Finder',
      partials: {
        header: 'partials/header',
        navigation: 'partials/navigation',
        footer: 'partials/footer'
      }
    }
  );
});

module.exports = router;
