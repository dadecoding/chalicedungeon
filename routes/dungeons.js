var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

router.get('/', function(req, res) {

  mongoose.model('dungeons').find(function(err, dungeonsList) {
    res.render('dungeons', {
      title: 'Bloodborne - Chalice Finder',
      pageTitle: 'Chalice Dungeons',
      dungeons: dungeonsList,
      partials: {
        header: 'partials/header',
        navigation: 'partials/navigation',
        footer: 'partials/footer'
      }
    });
  });

});

router.get('/:dungeonid', function(req, res) {
  mongoose.model('dungeons').find({ 'chalice_id': { $regex : ".*"+req.params.dungeonid+".*" } }, function(err, dungeonsList) {
    console.log(dungeonsList);
    res.render('dungeons', {
      title: 'Bloodborne - Chalice Finder',
      pageTitle: 'Chalice Dungeons',
      dungeons: dungeonsList,
      partials: {
        header: 'partials/header',
        navigation: 'partials/navigation',
        footer: 'partials/footer'
      }
    });
  });
});

module.exports = router;
